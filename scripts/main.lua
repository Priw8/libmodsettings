local PlayerList = require "necro.client.PlayerList"
local Event = require "necro.event.Event"
local Menu = require "necro.menu.Menu"
local Resources = require "necro.client.Resources"

-- This table stores the full data (setting names, keys, types etc) added with LibModSettings.add
-- Only the actual values are uploaded as resource. When downloaded, they are also inserted into this object.
local Settings = {}

local RESOURCE_ID = 42069 * 6969 -- Should be unique, hopefully

local function prepareUploadableSettings(settings)
    local res = {}
    for modname,  entries in pairs(settings) do
        res[modname] = {}
        local currentMod = res[modname]
        for i = 1, #entries do
            if entries[i].value ~= nil then
                currentMod[#currentMod+1] = entries[i].value
            end
        end
    end
    return res
end

local function loadDownloadedSettings(settingValues)
    for modname, entries in pairs(Settings) do
        local currentValues = settingValues[modname]
        local valueIndex = 1
        for i = 1, #entries do
            if entries[i].value ~= nil then
                entries[i].value = currentValues[valueIndex]
                valueIndex = valueIndex + 1
            end
        end
    end
    return Settings
end

local function uploadSettings(settings)
    Settings = settings
    local uploadable = prepareUploadableSettings(settings)
    Resources.upload(RESOURCE_ID, "LibModSettings", uploadable)
end

local function getSettings()
    local res = Resources.getData(RESOURCE_ID)
    return type(res) == "table" and loadDownloadedSettings(res) or Settings
end

local function getEntry(mod, index)
    local settings = getSettings()
    if settings[mod] and settings[mod][index] then
        return settings[mod][index]
    end
    return {
        label = "error",
        type = "number",
        value = 0
    }
end

local function setEntry(mod, index, val)
    local settings = getSettings()
    if settings[mod] and settings[mod][index] then
        settings[mod][index].value = val
        uploadSettings(settings)
    end
end

local LABELNAME = "Mod settings"

local function getLabelForSetting(mod, index)
    local entry = getEntry(mod, index)

    local txt = entry.name
    if entry.type == "select" then
        if entry.value > #entry.options then
            entry.value = 1
        end
        txt = txt .. ": " .. entry.options[entry.value]
    elseif entry.type ~= "label" then
        txt = txt .. ": " .. tostring(entry.value)
    else
        txt = "-- " .. txt .. " --"
    end

    if entry.postfix then
        txt = txt .. entry.postfix
    end

    return txt
end

local function settingChangeLeft(mod, index)
    local entry = getEntry(mod, index)
    if entry.type == "number" then
        entry.value = entry.value - (entry.increment or 1)
        if entry.min and entry.min > entry.value then
            entry.value = entry.min
        end
    elseif entry.type == "select" then
        entry.value = entry.value - 1
        if entry.value < 1 then
            entry.value = 1
        end
    elseif entry.type == "bool" then
        entry.value = not entry.value
    end
    setEntry(mod, index, entry.value)
end

local function settingChangeRight(mod, index)
    local entry = getEntry(mod, index)
    if entry.type == "number" then
        entry.value = entry.value + (entry.increment or 1)
        if entry.max and entry.max < entry.value then
            entry.value = entry.max
        end
    elseif entry.type == "select" then
        entry.value = entry.value + 1
        if entry.value > #entry.options then
            entry.value = #entry.options
        end
    elseif entry.type == "bool" then
        entry.value = not entry.value
    end
    setEntry(mod, index, entry.value)
end

local function openModSettings()
    Menu.open("LibModSettingsMain")
end

local function openSingleModSettings(name)
    Menu.open("LibModSettingsSingle", name)
end

Event.menu.add(
    "LibModSettingsSingle",
    "LibModSettingsSingle",
    function (ev)
        ev.menu = {}
        ev.menu.label = ev.arg
        ev.menu.entries = {}

        local entries = Settings[ev.arg]
        local isHost = PlayerList.getLocalPlayerID() == PlayerList.getHostPlayerID()
        for i = 1, #entries do
            if isHost and not (entries[i].type == "label") then
                ev.menu.entries[#ev.menu.entries+1] = {
                    label = function ()
                        return getLabelForSetting(ev.arg, i)
                    end,
                    leftAction = function ()
                        settingChangeLeft(ev.arg, i)
                    end,
                    rightAction = function ()
                        settingChangeRight(ev.arg, i)
                    end,
                    action = function ()

                    end
                }
            else
                ev.menu.entries[#ev.menu.entries+1] = {
                    label = function ()
                        return getLabelForSetting(ev.arg, i)
                    end
                }
            end
        end

        ev.menu.entries[#ev.menu.entries+1] = {
            label = "Done",
            action = Menu.close
        }
    end
)

Event.menu.add(
    "LibModSettingsMain",
    "LibModSettingsMain",
    function (ev)
        if PlayerList.getLocalPlayerID() == PlayerList.getHostPlayerID() then
            uploadSettings(Settings)
        end

        ev.menu = {}
        ev.menu.label = LABELNAME
        ev.menu.entries = {}

        for name in pairs(getSettings()) do
            ev.menu.entries[#ev.menu.entries+1] = {
                label = name,
                action = function ()
                    openSingleModSettings(name)
                end
            }
        end

        ev.menu.entries[#ev.menu.entries+1] = {
            label = "Done",
            action = Menu.close
        }
    end
)

local function findExistingEntry(entries)
    for i = 1, #entries do
        if entries[i].label == LABELNAME then
            return entries[i]
        end
    end
end

-- Add mod settings to the lobby menu
Event.menu.override(
    "lobbyMenu",
    1,
    function (func, ev)
        func(ev)
        -- Add entry after "mods"
        local entry = findExistingEntry(ev.menu.entries)
        if not entry then
            table.insert(ev.menu.entries, #ev.menu.entries, {
                label = LABELNAME,
                action = openModSettings
            })
        else
            -- Possibly update outdated option
            entry.action = openModSettings
        end
    end
)

local LibModSettings = {}

function LibModSettings.add(mod, entries)
    Settings[mod] = entries
    -- Settings can't be uploaded here...
    -- Why? It causes a circular dependency if something calls this function in top level!
    -- And it wouldn't make sense to upload them here, at load time, anyway
    -- After all, with the same mod setup, every player will get the same settings locally
    -- So it will default to the same thing for everyone. Upload only happens when settings are CHANGED from the defaults.
end

function LibModSettings.get(mod, name)
    local settings = getSettings()
    if settings[mod] then
        local entries = settings[mod]
        for i = 1, #entries do
            local entry = entries[i]
            local key = entry.key or entry.name
            if key == name then
                if entry.type == "select" then
                    if entry.value > #entry.options then
                        return entry.options[1]
                    end
                    return entry.options[entry.value]                    
                else
                    return entry.value
                end
            end
        end
    end
end

return LibModSettings
