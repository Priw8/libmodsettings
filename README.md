# DEPRECATED
LibModSettings will no longer receive any fixes or updates, even if it stops working on the latest version of Synchrony. Synchrony now has mod settings built in, so use that instead.

## LibModSettings

This mod adds a simple API for other mods to add settings. An extra "Mod settings" option will appear if you open the pause menu on the character selection screen. The usage is pretty simple:

```lua
local LibModSettings = require "LibModSettings.main"

LibModSettings.add("Your mod name", {
    {
        key = "myOptionKey", -- Key to be used to retrieve the value
        name = "My option", -- Name of the setting that's displayed in the menu (if key is not specified, this is also used as key)
        type = "number", -- Type of the value (explained below)
        value = 1 -- Default value
    }
})

-- Somewhere else in the code, when you want to read a value from the settings
local val = LibModSettings.get("You mod name", "myOptionKey")
```
Settings are automatically synced between players in online sessions, and only host can change them.

## Available setting types
### Number
Can be incremented/decremented by pressing left/right in the settings.
```lua
{
    key = "myOptionKey", 
    name = "My option",
    type = "number",
    min = 0, -- Optional: the smallest value that can be set. Defaults to nil (no min value)
    max = 5, -- Optional: the largest value that can be set. Defaults to nil (no max value)
    increment = 0.5, -- Optional: by how much the value is incremented/decremented when changed. Defaults to 1
    value = 1 -- Default value
}
```
### Bool
Toggles between `true` and `false`.
```lua
{
    key = "myOptionKey", 
    name = "My option",
    type = "bool",
    value = true -- Default value
}
```
### Select
Allows selecting from a group of string values. `LibModSettings.get` returns the string option currently selected (not the index).
```lua
{
    key = "myOptionKey", 
    name = "My option",
    type = "select",
    options = {"option 1", "option 2", "option 3"}
    value = 1 -- Default value - index in the options table
}
```
### Label
Label isn't a selectable option, but as the name slightly suggests... A label. It can be put above a group of related settings to make things a bit more organized.
```lua
{
    name = "My label",
    type = "label"
}
```
### Extra stuff
The optional `postfix` can be used to display some text after the option value. For example, the following will make the % character appear after the number:
```lua
{
    key = "myOptionKey", 
    name = "My option",
    type = "number",
    postfix = "%",
    min = 0,
    max = 100,
    increment = 5,
    value = 1
}
```

## Setting up as an optional dependency
To setup LibModSettings as an optional dependency, create some file like `settings.lua` in your mod:
```lua
local libModSettingsLoaded, LibModSettings = pcall(require, "LibModSettings.main")

local settingData = {
    {
        name = "General",
        type = "label"
    },
    {
        key = "useCustomHp",
        name = "Use custom starting hearts",
        type = "bool",
        value = false
    }
    -- Probably more options
}

local Settings = {}

if libModSettingsLoaded then
    LibModSettings.add("Crypt Arena", settingData)
    function Settings.get(key)
        return LibModSettings.get("Crypt Arena", key)
    end
else
    function Settings.get(key)
        for i = 1, #settingData do
            local entry = settingData[i]
            if entry.key and entry.key == key then
                if entry.type == "select" then
                    return entry.options[entry.value]
                else
                    return entry.value
                end
            end
        end
    end
end

return Settings
```
Then, you can `require` your `settings.lua` in other files, and use the `Settings.get` function. If LibModSettings is available, it will be used. If not, default settings will be returned.
